import { Routes, Route } from 'react-router-dom';
import NavBar from './components/shared/nav-bar/NavBar';
import Home from './pages/home/Home';
import CreateNews from './pages/create-news/CreateNews';
import EditNews from './pages/edit-news/EditNews';
import ShowNews from './pages/show-news/ShowNews';
import NewsCategory from './pages/news-category/NewsCategory';
import Login from './pages/login/Login';
import LoginPopUp from './pages/login/LoginPopUp';
import SignUp from './pages/sign-up/SignUp';
import NotFound from './pages/not-found/NotFound';
import Footer from './components/shared/footer/Footer';
import EditAvatar from './pages/edit-avatar/EditAvatar';
import Profile from './pages/profile/Profile';
import './index.css';

function App() {
    return (
        <div className='app'>
            <NavBar />
            <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/createnews' element={<CreateNews />} />
                <Route path='/news/:newsId/update' element={<EditNews />} />
                <Route path='/news/:newsId' element={<ShowNews />} />
                <Route
                    path='/newscategory/:categoryId'
                    element={<NewsCategory />}
                />
                <Route path='/editAvatar' element={<EditAvatar />} />
                <Route path='/profile' element={<Profile />} />
                <Route path='/login' element={<Login />} />
                <Route path='/loginpopup' element={<LoginPopUp />} />
                <Route path='/signup' element={<SignUp />} />
                <Route path='*' element={<NotFound />} />
            </Routes>
            <Footer />
        </div>
    );
}

export default App;
