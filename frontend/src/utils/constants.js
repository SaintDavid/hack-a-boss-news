export const EMAIL_REGEX =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const MAX_LENGTH_STRING = 60;

export const CURRENT_USER_LOCAL_STORAGE = 'currentUser';

export const HOST = 'http://localhost:8000';
export const HOSTCAT = 'http://localhost:8000/categories/';
