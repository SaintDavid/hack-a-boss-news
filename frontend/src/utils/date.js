export function formatDate(isoDate) {
  const date = new Date(isoDate);
  return date.toLocaleDateString();
}
