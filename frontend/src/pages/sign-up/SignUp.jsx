import { EMAIL_REGEX, MAX_LENGTH_STRING } from '../../utils/constants';
import Block from '../../components/shared/block/Block';
import Button from '../../components/shared/button/Button';
import InputText from '../../components/shared/inputs/InputText';
import InputPassword from '../../components/shared/inputs/InputPassword';
import InputArea from '../../components/shared/inputs/InputArea';
import useSignUp from './useSignUp';
import ErrorPopUp from '../../components/shared/error-pop-up/ErrorPopUp';

import './sign-up.css';

function SignUp() {
  const {
    state: { register, errors, passwordError, errorPopUp },
    actions: { handleSubmit, onSubmit, setErrorPopUp },
  } = useSignUp();

  return (
    <>
      <Block text='REGISTRO'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <ul className='signUpForm'>
            <li>
              <InputText
                label='NOMBRE'
                register={register('firstName', {
                  required: true,
                  maxLength: MAX_LENGTH_STRING,
                })}
                errors={errors}
                registerName='firstName'
                lengthmax={MAX_LENGTH_STRING}
              />
            </li>

            <li>
              <InputText
                label='APELLIDO'
                register={register('lastName', {
                  required: true,
                  maxLength: MAX_LENGTH_STRING,
                })}
                errors={errors}
                registerName='lastName'
                lengthmax={MAX_LENGTH_STRING}
              />
            </li>
            <li>
              <InputText
                label='EMAIL'
                register={register('email', {
                  required: true,
                  pattern: EMAIL_REGEX,
                })}
                errors={errors}
                registerName='email'
              />
            </li>
            <li>
              <InputArea
                label='BIOGRAFIA'
                register={register('bio', {
                  maxLength: 500,
                })}
                errors={errors}
                registerName='bio'
                lengthmax={500}
              />
            </li>
            <li>
              <InputPassword
                label='PASSWORD'
                register={register('password', {
                  required: true,
                  minLength: 8,
                })}
                errors={errors}
                registerName='password'
              />
            </li>
            <li>
              <InputPassword
                label='REPETIR PASSWORD'
                register={register('repeat-password', {
                  required: true,
                })}
                errors={errors}
                registerName='repeat-password'
              />
            </li>
            {passwordError && (
              <span className='error'>Las contraseñas no coinciden</span>
            )}
          </ul>
          <div className='buttonBox'>
            <Button text='ACEPTAR' />
          </div>
        </form>
      </Block>

      <ErrorPopUp open={errorPopUp} onClose={() => setErrorPopUp(false)} />
    </>
  );
}

export default SignUp;
