import Title from '../../components/shared/title/Title';
import Carousel from '../../components/carousel/Carousel';
import SingleCarousel from '../../components/singlecarousel/SingleCarousel';
import ShowCategories from '../../components/showCategories/ShowCategories';
import NewsEvent from '../../components/news-event/NewsEvent';
import useWindowWidth from '../../hooks/useWindowWidth';

import './home.css';

function Home() {
  const windowWidth = useWindowWidth();

  return (
    <>
      <div className='lastnews'>
        <Title text='ULTIMAS NOTICIAS' />
        {windowWidth > 860 ? <Carousel /> : <SingleCarousel />}
      </div>
      <div className='categories'>
        <Title text='CATEGORIAS' />
        <ShowCategories />
      </div>
      <div className='newpost'>
        <Title text='NUEVA NOTICIA' />
        <section className='new'>
          {windowWidth > 530 ? (
            <>
              <div className='image-container1'>
                <img src='./journalist1.png' alt='Imagen 1' />
              </div>
              <p className='create'>
                Añade tus noticias para que todo el mundo sepa que pasa a tu
                alrededor
              </p>
              <div className='image-container2'>
                <img src='./journalist2.png' alt='Imagen 2' />
              </div>
            </>
          ) : (
            <p className='create'>
              Añade tus noticias para que todo el mundo sepa que pasa a tu
              alrededor
            </p>
          )}
        </section>
        <div className='create-container'>
          <NewsEvent />
        </div>
      </div>
    </>
  );
}

export default Home;
