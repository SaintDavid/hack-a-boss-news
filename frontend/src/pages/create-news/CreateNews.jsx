import Block from '../../components/shared/block/Block';
import Button from '../../components/shared/button/Button';
import InputArea from '../../components/shared/inputs/InputArea';
import InputText from '../../components/shared/inputs/InputText';
import ErrorPopUp from '../../components/shared/error-pop-up/ErrorPopUp';
import { MAX_LENGTH_STRING } from '../../utils/constants';
import useCreateNews from './useCreateNews';
import { useAuth } from '../../context/authContext';
import './createNews.css';

const CreateNews = () => {
  const {
    state: { register, errors, errorPopUp, previewPhoto },
    actions: { handleSubmit, onSubmit, setErrorPopUp, handlePhotoChange },
  } = useCreateNews();

  const { categories } = useAuth();

  return (
    <>
      <Block text='NUEVA NOTICIA'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <ul className='createNewsForm'>
            <li>
              <InputText
                label='TITULO'
                register={register('title', {
                  required: true,
                  maxLength: MAX_LENGTH_STRING,
                })}
                errors={errors}
                registerName='title'
                lengthmax={MAX_LENGTH_STRING}
              />
            </li>
            <li>
              <label htmlFor='photo'>FOTO</label>
              {previewPhoto && (
                <img src={previewPhoto} alt='Preview' className='preview-img' />
              )}
              <input
                type='file'
                {...register('photo')}
                id='photo'
                onChange={handlePhotoChange}
                accept='image/*'
                className='file'
              />
              {errors.photo && <span>Debe seleccionar una imagen</span>}
            </li>
            <label htmlFor='categoryId'>CATEGORÍA</label>
            <select
              {...register('categoryId', { required: true })}
              id='categoryId'
              className='category'
            >
              <option value=''>--</option>
              {categories.map((cat) => {
                return (
                  <option key={cat.id} value={cat.id}>
                    {cat.name}
                  </option>
                );
              })}
            </select>
            {errors.categoryId?.type === 'required' && (
              <span className='error'>Campo requerido</span>
            )}
            <li>
              <InputArea
                label='ENTRADILLA'
                register={register('leadIn', {
                  maxLength: 300,
                })}
                errors={errors}
                registerName='leadIn'
                lengthmax={300}
              />
            </li>
            <li>
              <InputArea
                label='CONTENIDO'
                register={register('body')}
                errors={errors}
                registerName='body'
              />
            </li>
          </ul>
          <div className='buttonBox'>
            <Button text='CREAR NOTICIA' />
          </div>
        </form>
      </Block>

      <ErrorPopUp open={errorPopUp} onClose={() => setErrorPopUp(false)} />
    </>
  );
};

export default CreateNews;
