import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useAuth } from '../../context/authContext';
import { useNavigate } from 'react-router-dom';

import { HOST } from '../../utils/constants';

function useProfile() {
  const { updateProfile, user } = useAuth();

  const navigate = useNavigate();

  const [errorPopUp, setErrorPopUp] = useState(false);
  // Variable de estado para previsualizar la foto seleccionada
  const [avatarImg, setAvatarImg] = useState(`${HOST}/${user?.avatar}`);
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    setValue('firstName', user?.firstName);
    setValue('lastName', user?.lastName);
    setValue('bio', user?.bio);
    setValue('email', user?.email);
  }, []);

  const onSubmit = async (data) => {
    // Creamos un objeto de formData vacío
    const formData = new FormData();
    // Añadimos uno a uno los campos que necesitamos en backend
    formData.append('firstName', data.firstName);
    formData.append('lastName', data.lastName);
    formData.append('bio', data.bio);
    formData.append('email', data.email);

    // Agregamos el header para enviar form-data
    const config = {
      header: {
        'Content-Type': 'multipart/form-data',
      },
    };

    try {
      // Llamamos el servicio del signup con los parámetros esperados
      await updateProfile(formData, config);
      // Navegamos al dashboard
      navigate('/');
    } catch (error) {
      // Mostraremos nuestro pop up genérico de error
      setErrorPopUp(true);
    }
  };

  const handleOnChangeAvatar = (e) => {
    const target = e.target.files[0];
    const url = URL.createObjectURL(target);
    setAvatarImg(url);
  };

  return {
    state: { register, errors, errorPopUp, avatarImg },
    actions: { handleSubmit, onSubmit, setErrorPopUp, handleOnChangeAvatar },
  };
}

export default useProfile;
