import { string, object, number } from 'prop-types';
import './inputs.css';

function InputArea({ label, register, errors, registerName, lengthmax }) {
  return (
    <>
      <label>{label}</label>
      <textarea
        {...register}
        placeholder={lengthmax ? `max ${lengthmax}` : ''}
      />
      {errors[registerName]?.type === 'required' && (
        <span className='error'>Campo requerido</span>
      )}
      {errors[registerName]?.type === 'pattern' && (
        <span className='error'>Email inválido</span>
      )}
      {errors[registerName]?.type === 'maxLength' && (
        <span className='error'>{`Máximo ${lengthmax} caracteres`}</span>
      )}
    </>
  );
}

InputArea.propTypes = {
  label: string,
  register: object,
  errors: object,
  registerName: string,
  lengthmax: number,
};

export default InputArea;
