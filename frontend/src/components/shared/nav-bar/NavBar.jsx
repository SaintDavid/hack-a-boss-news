import { NavLink } from 'react-router-dom';
import { useAuth } from '../../../context/authContext';
import { HOST } from '../../../utils/constants';
import Button from '../button/Button';
import './nav-bar.css';

function NavBar() {
  const { isAuthenticated, user, logOut } = useAuth();
  return (
    <nav className='header-container'>
      <div>
        <NavLink to='/' className='HOME'>
          <img src='/LOGO.svg' alt='Home' />
        </NavLink>
      </div>
      {isAuthenticated ? (
        <>
          <div className='loged'>
            <NavLink to='editAvatar' className='avatar'>
              <img src={`${HOST}/${user.avatar}`} alt='Avatar' />
            </NavLink>
            <NavLink to='profile' className='profile'>
              {`${user.firstName} ${user.lastName}`}
            </NavLink>
            <div className='logout'>
              <Button text='Cerrar sesion' onClick={logOut} />
            </div>
          </div>
        </>
      ) : (
        <div className='user-actions'>
          <NavLink to='login' className='login-link'>
            ¿Tienes una cuenta? Inicia sesion
          </NavLink>
          <NavLink to='signup' className='register-link'>
            Register
          </NavLink>
        </div>
      )}
    </nav>
  );
}

export default NavBar;
