import { number, func } from 'prop-types';
import { useAuth } from '../../../context/authContext';

import './likes.css';

const Likes = ({
  newsId,
  positive,
  negative,
  likedByMe,
  dislikedByMe,
  handleNewsVotes,
}) => {
  // Si no estamos logueados no deberíamos poder votar. Para ello importamos user.
  const { isAuthenticated } = useAuth();

  return (
    <section className='likes'>
      <div>
        <img
          src={likedByMe ? '/like.svg' : '/megusta.svg'}
          alt='likeIcon'
          className='like-btn'
          // Si estamos logueados y existe "handleNewVotes" permitimos votar.
          onClick={() =>
            isAuthenticated && handleNewsVotes && handleNewsVotes(1, newsId)
          }
        />
        <p>{positive}</p>
      </div>
      <div>
        <img
          src={dislikedByMe ? '/dislike.svg' : '/disgusto.svg'}
          alt='dislikeIcon'
          className='dislike-btn'
          onClick={() =>
            // Si estamos logueados y existe "handleNewVotes" permitimos votar.
            isAuthenticated && handleNewsVotes && handleNewsVotes(0, newsId)
          }
        />
        <p>{negative}</p>
      </div>
    </section>
  );
};

Likes.propTypes = {
  newsId: number,
  positive: number,
  negative: number,
  likedByMe: number,
  dislikedByMe: number,
  handleNewsVotes: func,
};

export default Likes;
