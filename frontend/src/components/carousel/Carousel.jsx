import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getNews } from '../../services';
import { HOST } from '../../utils/constants';
import './carousel.css';

function Carousel() {
  const [startIndex, setStartIndex] = useState(0);
  const [news, setNews] = useState([]);

  useEffect(() => {
    getNews()
      .then((response) => {
        setNews(response.data.data.news);
      })
      .catch((error) => {
        console.error('Error al obtener las noticias:', error);
      });
  }, []);

  const handleNext = () => {
    const lastIndex = news.length - 1;
    if (startIndex + 2 <= lastIndex) {
      setStartIndex(startIndex + 2);
    } else {
      setStartIndex(0);
    }
  };

  const handlePrevious = () => {
    if (startIndex - 2 >= 0) {
      setStartIndex(startIndex - 2);
    } else {
      setStartIndex(news.length - 1);
    }
  };

  let currentNews = news[startIndex];
  let nextNews = news[startIndex + 1];

  if (news.length === 0) {
    return <p>No hay noticias que mostrar</p>;
  }

  return (
    <>
      <div className='carousel-container'>
        <Link to={`/news/${currentNews.id}`}>
          <div
            className='news-container'
            style={
              currentNews && currentNews.photo
                ? {
                    backgroundImage: `url(${HOST}/${currentNews.photo})`,
                  }
                : {
                    backgroundImage: `url(${HOST}/default-image.jpg)`,
                  }
            }
          >
            {currentNews ? (
              <>
                <h3>{currentNews.title}</h3>
                <p>{currentNews.leadIn}</p>
              </>
            ) : (
              <h4>Loading...</h4>
            )}
          </div>
        </Link>
        {nextNews && (
          <Link to={`/news/${nextNews.id}`}>
            <div
              className='news-container'
              style={
                nextNews && nextNews.photo
                  ? {
                      backgroundImage: `url(${HOST}/${nextNews.photo})`,
                    }
                  : {
                      backgroundImage: `url(${HOST}/default-image.jpg)`,
                    }
              }
            >
              {nextNews ? (
                <>
                  <h3>{nextNews.title}</h3>
                  <p>{nextNews.leadIn}</p>
                </>
              ) : (
                <h4>Loading...</h4>
              )}
            </div>
          </Link>
        )}
      </div>
      <div className='button-container'>
        <img
          src='./prev.svg'
          alt='Botón Prev'
          onClick={handlePrevious}
          className='prev-button'
        />
        {startIndex + 2 <= news.length ? (
          <p>{`${startIndex + 1} y ${startIndex + 2} de ${news.length}`}</p>
        ) : (
          <p>{`${startIndex + 1} de ${news.length}`}</p>
        )}
        <img
          src='./next.svg'
          alt='Botón Next'
          onClick={handleNext}
          className='next-button'
        />
      </div>
    </>
  );
}

export default Carousel;
