import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getNews } from '../../services';
import { HOST } from '../../utils/constants';
import './singleCarousel.css';

function SingleCarousel() {
  const [index, setIndex] = useState(0);
  const [news, setNews] = useState([]);

  useEffect(() => {
    getNews()
      .then((response) => {
        setNews(response.data.data.news);
      })
      .catch((error) => {
        console.error('Error al obtener las noticias:', error);
      });
  }, []);

  const handleNext = () => {
    const lastIndex = news.length - 1;
    if (index < lastIndex) {
      setIndex(index + 1);
    } else {
      setIndex(0);
    }
  };

  const handlePrevious = () => {
    if (index > 0) {
      setIndex(index - 1);
    } else {
      setIndex(news.length - 1);
    }
  };

  let currentNews = news[index];
  if (news.length === 0) {
    return <p>No hay noticias que mostrar</p>;
  }

  return (
    <>
      <div className='single-carousel-container'>
        <Link to={`/news/${currentNews.id}`}>
          <div
            className='single-news-container'
            style={
              currentNews && currentNews.photo
                ? { backgroundImage: `url(${HOST}/${currentNews.photo})` }
                : { backgroundImage: `url(${HOST}/default-image.jpg)` }
            }
          >
            {currentNews ? (
              <>
                <h3>{currentNews.title}</h3>
                <p>{currentNews.leadIn}</p>
              </>
            ) : (
              <h4>Loading...</h4>
            )}
          </div>
        </Link>
      </div>
      <div className='button-container2'>
        <img
          src='./prev.svg'
          alt='Botón Prev'
          onClick={handlePrevious}
          className='prev-button2'
        />
        <p>{`${index + 1} de ${news.length}`}</p>
        <img
          src='./next.svg'
          alt='Botón Next'
          onClick={handleNext}
          className='next-button2'
        />
      </div>
    </>
  );
}

export default SingleCarousel;
