import { NavLink } from 'react-router-dom';
import { useAuth } from '../../context/authContext';
import './newsEvent.css';

const NewsEvent = () => {
  const { isAuthenticated } = useAuth();

  return (
    <div className='container'>
      <NavLink
        to={isAuthenticated ? '/createnews' : '/login'}
        className='navLink'
      >
        <button className='createNews'>+Crear Noticia</button>
      </NavLink>
    </div>
  );
};

export default NewsEvent;
