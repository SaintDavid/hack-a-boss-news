import axios from 'axios';
import { HOST } from '../utils/constants';

export const getNews = (categoryId) => {
  const url = categoryId
    ? `${HOST}/news?categoryId=${categoryId}`
    : `${HOST}/news`;

  return axios.get(url);
};

export const getNewsById = (newsId) => {
  const url = `${HOST}/news/${newsId}`;

  return axios.get(url);
};

export const getCategories = () => {
  return axios.get(`${HOST}/categories`);
};

export const createNews = (formData, config) => {
  return axios.post(HOST + '/news', formData, config);
};

export const updateNews = async (formData, newsId) => {
  const res = await axios.put(HOST + `/news/${newsId}`, formData);

  return res;
};

export const voteNews = async (likes, newsId) => {
  const res = await axios.post(HOST + `/news/${newsId}/votes`, {
    likes,
  });

  return res.data;
};

export const deleteNews = async (newsId) => {
  await axios.delete(HOST + `/news/${newsId}`);
};
