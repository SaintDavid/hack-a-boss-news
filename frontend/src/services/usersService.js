import axios from 'axios';
import { HOST } from '../utils/constants';

export const login = (email, password) => {
  return axios.post(HOST + '/users/login', {
    email,
    password,
  });
};

export const signUp = (firstName, lastName, email, password, bio) => {
  return axios.post(HOST + '/users', {
    firstName,
    lastName,
    email,
    password,
    bio,
  });
};

export const updateAccount = (formData, config) => {
  return axios.put(HOST + '/users', formData, config);
};

export const updateAvatar = (formData, config) => {
  return axios.put(HOST + '/users', formData, config);
};
