// Importamos los hooks.
import { useEffect, useState } from 'react';

// Importamos los servicios.
import { getNews } from '../services';

const useNews = (categoryId) => {
  const [news, setNews] = useState();

  useEffect(() => {
    const fetchNews = async () => {
      const response = await getNews(categoryId);

      setNews(response.data.data.news);
    };

    fetchNews();
  }, [categoryId]);

  return { news };
};

export default useNews;
