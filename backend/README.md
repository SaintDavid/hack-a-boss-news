# Web de noticias colaborativas.

- Implementar una API que permita gestionar noticias colaborativas, estilo reddit o menéame,
- donde los usuarios puedan registrarse y publicar una noticia en una serie de categorías
- temáticas fijas.

## Instalar

- Instalar las dependencias mediante el comando `npm install` o `npm i`.

- Guardar el archivo `.env.example` como `.env` y cubrir los datos necesarios.

- Ejecutar `npm run initDB` para crear la base de datos y las tablas necesarias.

- Ejecutar `npm run dev` o `npm start` para lanzar el servidor.

- Importar la colección de Postman para poder probar los endpoints.

## Base de datos

- users
  id
  firstName
  lastName
  email
  password
  bio
  avatar
  createdAt
  modifiedAt

- news
  id
  title
  leadIn
  body (texto de la noticia)
  categoryId
  photo  
  userId
  createdAt
  modifiedAt

- categories
  id  
  name
  createdAt

- votes
  id
  likes (true o false)
  newsId
  userId
  createdAt

## Programmers

Jess - J
David - D
Sergio - S
Guan - G

## Endpoints del usuario ✅

- **POST** - [`/users`] - Registro de un usuario ✅
- **POST** - [`/users/login`] - Logea a un usuario ➡️ `Token` ✅
- **PUT** - [`/users`] - Permite actualizar información del usuario. ➡️ `Token` ✅
- **GET** - [`/users`] - Recupera la información propia del usuario. ➡️ `Token` ✅
- **GET** - [`/users/:userId`] - Recupera la información pública del usuario. ➡️ `Token` ✅

## Endpoints de news ✅

- **POST** - [`/news`] - Crea una noticia. ➡️ `Token` ✅
- **GET** - [`/news`] - Retorna el listado de noticias (puede filtrar por categoría). ✅
- **GET** - [`/news/:newsId`] - Retorna una noticia en concreto. ✅
- **POST** - [`/news/:newsId/votes] - Vota una noticia. ➡️ `Token`
  (si viene vote=0 negativo y vote=1 positivo) ✅
- **PUT** - [`/news/:newsId`] - Permite modificar una noticia (titulo, entradilla, categoria, texto, foto) Sólo el propietario ➡️ `Token` ✅
- **DELETE** - [`news/:newsId`] - Borra una noticia (solo el propietario puede) ➡️ `Token` ✅
- **GET** - [`news/categories`] - Devuelve la lista de categorías. ✅
