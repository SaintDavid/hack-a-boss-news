const getDB = require('../../db/getDB');

const generateError = require('../../services/generateError');

const searchCategory = async (categoryId) => {
  let connection;

  try {
    connection = await getDB();

    // Buscamos en la DB las categorias registradas.
    let [category] = await connection.query(
      `SELECT name FROM categories WHERE id = ?`,
      [categoryId]
    );

    // Si no existe la categoria arrojamos error.
    if (category.length < 1) {
      generateError('La categoria no existe', 404);
    }

    return category[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = searchCategory;
