const getDB = require('../../db/getDB');

const generateError = require('../../services/generateError');

const deleteNewsQuery = async (newsId, userId = 0) => {
  let connection;

  try {
    connection = await getDB();

    const [news] = await connection.query(
      `
          SELECT id, userId FROM news WHERE id = ?           
      `,
      [newsId]
    );

    // Si no encontramos la noticia mandamos de vuelta el mensaje de error.
    if (news.length < 1) generateError('Noticia no encontrada', 404);

    // Si el usuario no es el propietario no puede eliminar la noticia.
    if (userId !== news[0].userId) generateError('No tiene autorización', 401);

    // Llegados aquí significa que ha encontrado la noticia y que el usuario
    // es el propietario por lo que borramos tanto los votos como la noticia.
    await connection.query(
      `
          DELETE FROM votes WHERE newsId = ?
      `,
      [newsId]
    );

    await connection.query(
      `
          DELETE FROM news WHERE id = ?
      `,
      [newsId]
    );

    return news;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteNewsQuery;
