const getDB = require('../../db/getDB');

const generateError = require('../../services/generateError');

const userEmailExist = async (email) => {
  let connection;

  try {
    connection = await getDB();

    // Buscamos en la base de datos algún usuario con ese email.
    let [users] = await connection.query(
      `SELECT id FROM users WHERE email = ?`,
      [email]
    );

    // Si existe algún usuario con ese email lanzamos un error.
    if (users.length > 0) {
      generateError('Ya existe un usuario con ese email', 403);
    }

    // Dado que no puede existir más de un usuario con el mismo email, en caso de que en el
    // array de usuarios haya un usuario estará en la posición 0.
    return users[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = userEmailExist;
