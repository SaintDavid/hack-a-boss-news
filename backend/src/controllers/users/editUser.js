const selectUserByIdQuery = require('../../models/users/selectUserByIdQuery');
const updateUserQuery = require('../../models/users/updateUserQuery');
const userEmailExist = require('../../models/users/userEmailExist');

const editUserSchema = require('../../schemas/editUserSchema');
const validateSchema = require('../../services/validateSchema');

const generateError = require('../../services/generateError');
const deletePhoto = require('../../services/deletePhoto');
const savePhoto = require('../../services/savePhoto');

const editUser = async (req, res, next) => {
  try {
    let { firstName, lastName, email, bio } = req.body;

    //Comprobamos si manda avatar.
    let newAvatar;
    if (req.files?.avatar) {
      newAvatar = req.files.avatar;
    }

    // Puede modificar cualquiera de estos campos, puede modificar 1 o los 5
    if (!firstName && !lastName && !email && !bio && !newAvatar) {
      generateError('Faltan campos', 400);
    }

    // Obtenemos los datos del usuario.
    const user = await selectUserByIdQuery(req.user.id);

    // En caso de que el usuario no haya enviado algún dato, establezco
    // como valor por defecto lo que haya guardado en la base de datos.
    firstName = firstName || user.firstName;
    lastName = lastName || user.lastName;
    email = email || user.email;
    bio = bio || user.bio;
    avatar = user.avatar;

    // Validamos los datos del body con joi.
    await validateSchema(editUserSchema, {
      firstName,
      lastName,
      email,
      bio,
      avatar: newAvatar,
    });
    // Comprobamos que no existe otro usuario con el mismo email.
    if (email !== user.email) await userEmailExist(email);

    // Si nos manda avatar para modificarlo, borramos el antiguo si hay y
    // preparamos el nuevo para incluirlo en el registro.
    if (newAvatar) {
      if (avatar) await deletePhoto(avatar);

      avatar = await savePhoto(newAvatar, 100);
    }

    // Actualizamos los datos del usuario.
    await updateUserQuery(firstName, lastName, email, bio, avatar, req.user.id);

    res.send({
      status: 'ok',
      message: 'Usuario actualizado',
      firstName,
      lastName,
      email,
      bio,
      avatar,
    });
  } catch (err) {
    next(err);
  }
};

module.exports = editUser;
