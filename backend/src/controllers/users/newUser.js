const insertUserQuery = require('../../models/users/insertUserQuery');
const newUserSchema = require('../../schemas/newUserSchema');
const savePhoto = require('../../services/savePhoto');
const userEmailExist = require('../../models/users/userEmailExist');

const validateSchema = require('../../services/validateSchema');

const newUser = async (req, res, next) => {
  try {
    const { firstName, lastName, email, password, bio } = req.body;

    let avatar;
    if (req.files?.avatar) {
      avatar = req.files.avatar;
    }

    await validateSchema(newUserSchema, {
      firstName,
      lastName,
      email,
      password,
      bio,
      avatar,
    });

    // Comprobamos que no existe otro usuario con el mismo email.
    await userEmailExist(email);

    if (avatar) {
      avatar = await savePhoto(avatar, 100);
    }

    await insertUserQuery(firstName, lastName, email, password, bio, avatar);

    res.send({
      status: 'ok',
      message: 'Usuario creado',
      user: {
        firstName,
        lastName,
        email,
        bio,
        avatar,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = newUser;
