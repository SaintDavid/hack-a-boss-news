const createNews = require('./createNews');
const listNews = require('./listNews');
const getNews = require('./getNews');
const deleteNews = require('./deleteNews');
const updateNews = require('./updateNews');
const newVote = require('./newVote');
const getCategories = require('./getCategories');

module.exports = {
  createNews,
  listNews,
  getNews,
  deleteNews,
  updateNews,
  newVote,
  getCategories,
};
