const deleteNewsQuery = require('../../models/news/deleteNewsQuery');

const deleteNews = async (req, res, next) => {
  try {
    const { newsId } = req.params;

    // Dado que la propiedad user puede no existir lo indicamos por medio de la interrogación.
    const news = await deleteNewsQuery(Number(newsId), req.user?.id);

    res.send({
      status: 'ok',
      data: 'Noticia eliminada',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = deleteNews;
