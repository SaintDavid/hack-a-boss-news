const joi = require('joi');

const newUserSchema = joi.object().keys({
  firstName: joi
    .string()
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere un nombre');
      } else {
        return new Error('El nombre no es válido');
      }
    }),

  lastName: joi
    .string()
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere mínimo un apellido');
      } else {
        return new Error('El apellido no es válido');
      }
    }),

  email: joi
    .string()
    .email()
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere un email');
      } else {
        return new Error('El email no es válido');
      }
    }),

  password: joi
    .string()
    .min(8)
    .max(100)
    .regex(
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[¡!$%^&*()_+|~=`{}:";'<>¿?,.])[a-zA-Z0-9¡!$%^&*()_+|~=`{}:";'<>¿?,.]{8,}$/
    )
    .error((errors) => {
      switch (errors[0].code) {
        case 'any.required':
          return new Error('Se requiere una contraseña');

        case 'string.pattern.base':
          return new Error(
            'La contraseña debe tener al menos una letra mayúscula, una letra minúscula y un signo de puntuación'
          );

        default:
          return new Error('La contraseña debe tener entre 8 y 100 caracteres');
      }
    }),

  bio: joi.string().optional().min(0),
  avatar: joi.object().optional(),
});

module.exports = newUserSchema;
