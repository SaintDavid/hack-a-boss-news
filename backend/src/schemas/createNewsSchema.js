const joi = require('joi');

const createNewsSchema = joi.object().keys({
  title: joi
    .string()
    .max(60)
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere un título');
      } else if (errors[0].code === 'string.max') {
        return new Error('El título debe tener como máximo 60 caracteres');
      } else {
        return new Error('El título no es válido');
      }
    }),

  leadIn: joi
    .string()
    .max(300)
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere una entradilla');
      } else if (errors[0].code === 'string.max') {
        return new Error('La entradilla debe tener como máximo 300 caracteres');
      } else {
        return new Error('La entradilla no es válida');
      }
    }),

  body: joi
    .string()
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere el texto de la noticia');
      } else {
        return new Error('El texto de la noticia no es válido');
      }
    }),

  categoryId: joi
    .number()
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere la categoría de la noticia');
      } else {
        return new Error('La categoría de la noticia no es válida');
      }
    }),

  photo: joi.object().optional(),
});

module.exports = createNewsSchema;
